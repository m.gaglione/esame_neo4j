from neo4j import GraphDatabase
import traceback


class neo4jDB:
    def __init__(self):
        try:
            self.driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "password"))
            print(f"connessione al database avvenuta con successo")
        except Exception as e:
            print(f'errore di connessione al db')


    def menu(self):

        print(f"Benvenuto nel nostro programma di gestione dei percorsi montani! \n"
              f"Potrai scegliere due differenti località e la modalità di percorso, il programma ti consiglierà la via migliore.\n"
              f"Puoi scegliere tra:\n"
              f"1) percorso in bici\n"
              f"2) percorso più veloce\n"
              f"3) percorso più facile")

        session = self.driver.session()

        località_disponibili = ['localitaBanana', 'localitaPrugna', 'localitaMela', 'localitaArancia',
                                'localitaDattero', 'localitaMango', 'localitaCacomela', 'localitaAnanas']

        flag = True

        while flag == True:

            punto_di_partenza = input(f'Inserire punto di partenza: ')
            punto_di_arrivo = input(f'Inserire punto di arrivo: ')

            if punto_di_partenza in località_disponibili and punto_di_arrivo in località_disponibili:
                print(f'hai scelto {punto_di_partenza} come punto di partenza e {punto_di_arrivo} come punto di arrivo')
                flag = False
                if punto_di_partenza == punto_di_arrivo:
                    print('Hai scelto la stessa localita, cerca di prestare più attenzione!')
            else:
                print('spiacente, non hai scelto correttamente una o entrambe le localita.')

        while flag == False:
            bici = input(f'Desideri percorrere il percorso in bici o a piedi?: B / P : ')
            if bici.upper() == 'B' or bici.upper() == 'P':
                if bici.upper() == 'P':
                    bici = 'il percorso a piedi'
                else:
                    bici = 'il percorso in bici'
                print(f'Hai scelto {bici}')
                flag = True
            else:
                print("risposta non valida, riprova.")

        while flag == True:
            criterio = input(f'preferisci fare il percorso più veloce o più facile?  V / F  : ')
            if criterio.upper() == 'V' or criterio.upper() == 'F':
                print(f"Hai scelto il percorso {criterio}")
                if criterio == 'V':
                    criterio = 'difficolta_tot'
                else: criterio = 'tempo_tot'
                flag = False
            else:
                print("risposta non valida, riprova")

    def query(self):

        session = self.driver.session()

        # query modello
     #   query = f"match percorso = (o {nome : {punto_di_partenza}})-[r*]->(d {nome: {punto_di_arrivo}}) return percorso limit 10 orderby {criterio}"


        query_esempio = "match percorso = (o {nome: 'localitaBanana'})-[r*3]->(d {nome : 'c'}) return percorso limit 5"

        result = session.run(query_esempio)

        for record in result:
            print(record)

    def close(self):
        self.driver.close()

if __name__ == "__main__":

    neo4jDB = neo4jDB()
    neo4jDB.menu()
    neo4jDB.query()
    neo4jDB.close()

